# ocrdetect

**ocrdetect** jest skryptem wykrywającym tekst w plikach pdf. Skrypt rozróżnia pliki zawierające tekst od plików, w których występują tylko zdjęcia/skany i przenosi je do odpowiednich katalogów.

## Zależności

- pdfgrep

## Instalacja

1. Zainstaluj `pdfgrep`.

```bash
# Debian/Ubuntu
sudo apt install pdfgrep
```
 Lub pobierając i instalując [pakiet pdfgrep](https://packages.debian.org/search?keywords=pdfgrep) ręcznie.

2. Skopiuj skrypt `ocrdetect.sh` do wybranego katalogu.

3. Skopiuj plik konfiguracyjny `ocrdetect.conf` do katalogu `/etc/ocrdetect`.

4. Utwórz katalogi dla plików wyjściowych.
> Możesz podać inną scieżkę dla bazowego katalogu wyjściowego `output`. Pamiętaj, aby zmienić ją również w konfiguracji skryptu.
```bash
mkdir output output/pdf2ocr output/pdf2txt output/pdf2err
```

## Konfiguracja

Konfiguracja skryptu odbywa się za pomocą pliku `ocrdetect.conf`.

Wymagane:

`THRESHOLD` - Minimalna liczba słów w dokumencie, aby został skategoryzowany jako dokument zawierający tekst.  
`OUTPUT_PATH` - Ścieżka do katalogu wyjściowego.

Opcjonalne:

`LOG_FILE` - Ścieżka do pliku, w którym będą zapisywane logi dodatkowo poza serwisem syslog.
## Korzystanie

1. Użycie jednorazowe.
> Ścieżki mogą być względne (relative) lub bezwględne (absolute).
```bash
bash ocrdetect.sh path/to/input/directory
```

2. CRON.
> Ścieżki muszą być bezwględne (absolute).
```bash
crontab -e

# Przykład
15 14 1 * * /path/to/ocrdetect.sh /path/to/input/directory
```

> Duplikaty plików nie zostają nadpisywane i pozostają w katalogu wejściowym.