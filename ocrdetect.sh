#!/bin/bash

# Load config
source /etc/ocrdetect/ocrdetect.conf

function log () {
    logger -t ocrdetect -p user.$1 $2
    if [ ! -z "$LOG_FILE" ]
    then
        echo "$(date +%Y-%m-%d--%H-%M-%S) ${1^^} ocrdetect: $2" >> $LOG_FILE
    fi
}

function terminate_if_running {
    SCRIPT_NAME=`/bin/basename $1`
    
    for PID in `/bin/ps -fC $SCRIPT_NAME | /bin/grep "^$USER" | /usr/bin/awk '{print $2}'`
    do
            ALIVE=`/bin/ps -p $PID -o pid=`
            if [[ "$PID" != "$$"  &&  "$ALIVE" != "" ]]
            then
                    log warning "$SCRIPT_NAME is already running as process $PID! Terminating..."
                    exit 0
            fi
    done
}

terminate_if_running $0

function process_pdfs () {
    for FILE in $@
    do
        WORD_COUNT=$(pdfgrep --count '\w+' $FILE)

        if [ $? -eq 2 ] 
        then 
            mv -n $FILE "$OUTPUT_PATH"/pdf2err
            log error "An error occured while checking the pdf file."
            exit 1
        else 
            if [ $WORD_COUNT -gt $THRESHOLD ]
            then
                mv -n $FILE "$OUTPUT_PATH"/pdf2txt
            else
                mv -n $FILE "$OUTPUT_PATH"/pdf2ocr
            fi

            log info "Successfully moved: $FILE. Word count: $WORD_COUNT."
        fi
    done
}

FILES=$(find $@/*.pdf 2>/dev/null) 

if [ $? -eq 0 ] 
then 
    process_pdfs $FILES
else
    log warning "An error occured while getting input files. Probably the are no pdf files in the input directory. Skipping check."
    exit 0
fi
